package aero.employee.controller;

import aero.employee.model.Flight;
import aero.employee.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController("flightController")
@RequestMapping("/flight")
public class FlightController {

    @Autowired
    private FlightRepository repository;

    @GetMapping
    public ResponseEntity<List<Flight>> getFlights(){
        return new ResponseEntity(repository.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{flightId}")
    public void delete(
            @PathVariable("flightId") int flightId
    ){

        repository.deleteById(flightId);
    }

    @PostMapping
    public void insert(@RequestBody Flight flight){
        repository.saveAndFlush(flight);
    }
}
