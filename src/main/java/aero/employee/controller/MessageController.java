package aero.employee.controller;

import aero.employee.model.Message;
import aero.employee.repository.MessageRepository;
import aero.employee.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController("messageController")
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageRepository repository;

    @Autowired
    private EmailService service;
    @GetMapping
    public ResponseEntity<List<Message>> getMessages(){
        return new ResponseEntity(repository.findAll(), HttpStatus.OK);
    }
    @GetMapping("/new")
    public ResponseEntity<List<Message>> getNewMessages(){
        return new ResponseEntity(repository.findAllByIsOpened(false), HttpStatus.OK);
    }
    @PostMapping
    public void sendMessage(
            @RequestBody Map<String,String> data
    ){
        service.sendEmail(data.get("content"),data.get("email"));
    }
    @PutMapping("/{id}")
    public  void setOpened(
            @PathVariable("id") int id
    ){
        Message message = repository.findById(id).get();
        message.setOpened(true);
        repository.saveAndFlush(message);
    }
}
