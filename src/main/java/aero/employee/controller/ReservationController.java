package aero.employee.controller;

import aero.employee.model.Reservation;
import aero.employee.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController("reservationController")
@RequestMapping("/reservation")
public class ReservationController {
    @Autowired
    private ReservationRepository repository;

    @GetMapping("/{param}")
    public ResponseEntity<List<Reservation>> getReservations(
            @PathVariable("param") String param
    ){
        if(param.equals("new"))
            return new ResponseEntity(repository.findAllByStatusContaining("nova"), HttpStatus.OK);
        else if(param.equals("rejected"))
            return new ResponseEntity(repository.findAllByStatusContaining("odbijena"), HttpStatus.OK);
        else if(param.equals("accepted"))
            return new ResponseEntity(repository.findAllByStatusContaining("odobrena"), HttpStatus.OK);
        else
            return new ResponseEntity(repository.findAll(), HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public void update(
            @PathVariable("id") int id,
            @RequestBody Map<String,String> data
    ){
        Reservation reservation = repository.getById(id);
        if(data.get("type").equals("odobrena"))
            reservation.setStatus("odobrena");
        else if(data.get("type").equals("odbijena")) {
            reservation.setStatus("odbijena");
            reservation.setReason(data.get("reason"));
            System.out.println(data.get("reason"));
        }
        repository.saveAndFlush(reservation);
    }
}
