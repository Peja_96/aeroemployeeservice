package aero.employee.model;

import javax.persistence.*;
import java.security.PrivateKey;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table (name = "flight", schema = "ip")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "start_location")
    private String startLocation;
    @Basic
    @Column(name = "destination")
    private String destination;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "type")
    private String type;
    @Basic
    @Column(name = "time")
    private Timestamp date;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }



    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", startLocation='" + startLocation + '\'' +
                ", destination='" + destination + '\'' +
                ", status='" + status + '\'' +
                ", type='" + type + '\'' +
                ", date=" + date +
                '}';
    }
}
