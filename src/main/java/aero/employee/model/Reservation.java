package aero.employee.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "reservation", schema = "ip")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private int id;
    @Column(name = "date")
    private Date date;
    @Column(name = "start_location")
    private String startLocation;
    @Column(name = "destination")
    private String destination;
    @Column(name = "seat_number")
    private int seatNumber;
    @Column(name = "specification_file")
    private String specificationFile;
    @Column(name = "load_description")
    private String loadDescription;
    @Column(name = "status")
    private String status;
    @Column(name = "reason")
    private String reason;
    @OneToOne(targetEntity = Person.class)
    @JoinColumn(name = "user_id")
    private Person person;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getSpecificationFile() {
        return specificationFile;
    }

    public void setSpecificationFile(String specificationFile) {
        this.specificationFile = specificationFile;
    }

    public String getLoadDescription() {
        return loadDescription;
    }

    public void setLoadDescription(String loadDescription) {
        this.loadDescription = loadDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", date=" + date +
                ", startLocation='" + startLocation + '\'' +
                ", destination='" + destination + '\'' +
                ", seatNumber=" + seatNumber +
                ", specificationFile='" + specificationFile + '\'' +
                ", loadDescription='" + loadDescription + '\'' +
                ", status='" + status + '\'' +
                ", reason='" + reason + '\'' +
                ", person=" + person +
                '}';
    }
}
