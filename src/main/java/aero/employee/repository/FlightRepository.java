package aero.employee.repository;

import aero.employee.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("flightRepository")
public interface FlightRepository extends JpaRepository<Flight,Integer> {

}
